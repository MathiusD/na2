default_target: deploy

start_debug:
	@export FLASK_ENV=development
	@flask run -p 8000 -h 0.0.0.0

start:
	@echo "WebSite Launch"
	@flask run -p 8000 -h 0.0.0.0

deploy: rebuild start

build:
	@pip install -r requirements.txt

clear_log:
	@rm logs/*.log

clear:
	@echo "Clear Repo"

rebuild: clear update build

update:
	@git pull

docker_update: update
	@docker-compose build

docker_force_update: update
	@docker-compose build --no-cache

docker_up: docker_update docker_start

docker_start:
	@docker-compose up -d

docker_down:
	@docker-compose down

docker_restart: docker_down docker_start

docker_reload: docker_force_update docker_restart
