from flask import Flask, render_template, redirect
from config import settings

app = Flask(__name__)

@app.route("/generator")
def generator():
    return render_template("generator.jinja", uri=settings.HOST)

@app.route("/")
def index():
    return redirect("/generator")