/**
 * Class for represent Filter in NA2 viewer
 */
class Filter {
    /**
     * Constructor for Filter
     * @param {String} name String represent id
     * @param {String} desc String represent desc of this item
     * @param {Number} max Integer represent max value of this filter
     */
	constructor(name, desc, max) {
        this.name = name;
        this.desc = desc;
        this.max = max;
    }
    /**
     * Function for add label and input related to this filter
     * @param {HTMLElement} node 
     */
    addFilterToDOM(node) {
        var lbl = document.createElement("label");
        lbl.htmlFor = this.name;
        lbl.textContent = this.desc;
        var input = document.createElement("input");
        input.value = 0;
        input.type = "number";
        input.id = this.name;
        input.min = 0;
        input.max = this.max;
        input.onchange = function() {
            return drawSWFObj();
        };
        node.appendChild(lbl);
        node.appendChild(input);
    }
}

var filters = [
    new Filter("avatarType", "Type d'Avatar", 15),
    new Filter("squeletron", "Taille du Squelette", 9),
    new Filter("school", "École", 4),
    new Filter("lvl", "Niveau", 6),
    new Filter("uniformPattern", "Motif de l'Uniforme", 5),
    new Filter("backPattern", "Motif du Fond", 8),
    new Filter("spiritRelated", "Relatif aux esprits (Si type d'avatar = 12 ou 13 [Et potentiellement 14 ou 15])"),
    new Filter("hair", "Cheveux", 6),
    new Filter("eyes", "Yeux", 4),
    new Filter("noze", "Nez", 4),
    new Filter("mouth", "Bouche", 5),
    new Filter("ear", "Oreilles", 2),
    new Filter("clothesHighType", "Type de Vêtement Haut", 2),
    new Filter("clothesDownType", "Type de Vêtement Bas", 2),
    new Filter("headAdd", "Chapeau", 5),
    new Filter("drop", "Goutte (Si type d'avatar = 4)", 1),
    new Filter("skinColor", "Couleur de peau", 10),
    new Filter("hairColor", "Couleur des cheveux", 9),
    new Filter("eyesColor", "Couleur des yeux", 7),
    new Filter("uniformColor", "Couleur de l'uniforme", 3),
    new Filter("uniformBorderColor", "Couleur des bordures de l'uniforme", 3),
    new Filter("uniformPatternColor", "Couleur des motifs de l'uniforme", 3),
    new Filter("clothesHighColor", "Couleur des vêtements du Haut", 28),
    new Filter("clothesDownColor", "Couleur des vêtements du Bas", 28),
    new Filter("shoesColor", "Couleur des chaussures", 28),
    new Filter("pyramColor", "Couleur du Pyram sur la tête (Si Chapeau = 5)", 28),
    new Filter("backColorPattern", "Couleur du Motif du Fond", 28),
];

/**
 * Function for create app
 * @param {Boolean} onlyCode If form display only basic input for input string
 * @param {Boolean} inOrder If form is display in base order or in logical order
 */
function initSWFObj(onlyCode, inOrder) {
    var filtersDOM = document.getElementById("filters");
    filtersDOM.innerHTML = "";
    var pMenu = document.createElement("p");
    var bCode = document.createElement("button");
    bCode.textContent = "Formulaire par code";
    bCode.onclick = () => {
        return initSWFObj(true);
    };
    var bBtnDefault = document.createElement("button");
    bBtnDefault.textContent = "Formulaire par ordre de saisie";
    bBtnDefault.onclick = () => {
        return initSWFObj(false, true);
    };
    var bBtnBeautifull = document.createElement("button");
    bBtnBeautifull.textContent = "Formulaire par ordre cohérent";
    bBtnBeautifull.onclick = () => {
        return initSWFObj();
    };
    pMenu.appendChild(bCode);
    pMenu.appendChild(bBtnDefault);
    pMenu.appendChild(bBtnBeautifull);
    filtersDOM.appendChild(pMenu);
    if (onlyCode == true) {
        var pGlobal = document.createElement("p");
        var lbl = document.createElement("label");
        lbl.htmlFor = "global";
        lbl.textContent = "Saisissez un code";
        var input = document.createElement("input");
        input.id = "global";
        input.onchange = function() {
            return drawSWFObj(input.value);
        };
        pGlobal.appendChild(lbl);
        pGlobal.appendChild(input);
        filtersDOM.appendChild(pGlobal);
    } else {
        if (inOrder == true)
            filters.forEach(
                (filter) => {
                    var p = document.createElement("p");
                    filter.addFilterToDOM(p);
                    filtersDOM.appendChild(p);
                }
            );
        else {
            var pCosmetique = document.createElement("p");
            pCosmetique.textContent = "Cosmetiques :";
            var pType = document.createElement("p");
            filters[0].addFilterToDOM(pType);
            filters[1].addFilterToDOM(pType);
            var pHair = document.createElement("p");
            filters[7].addFilterToDOM(pHair);
            filters[17].addFilterToDOM(pHair);
            var pEyes = document.createElement("p");
            filters[8].addFilterToDOM(pEyes);
            filters[18].addFilterToDOM(pEyes);
            var pNoze = document.createElement("p");
            filters[9].addFilterToDOM(pNoze);
            var pMouth = document.createElement("p");
            filters[10].addFilterToDOM(pMouth);
            var pEar = document.createElement("p");
            filters[11].addFilterToDOM(pEar);
            var pSkinColor = document.createElement("p");
            filters[16].addFilterToDOM(pSkinColor);
            var pVestimentaire = document.createElement("p");
            pVestimentaire.textContent = "Vestimentaire :";
            var pSchool = document.createElement("p");
            filters[2].addFilterToDOM(pSchool);
            filters[3].addFilterToDOM(pSchool);
            var pLvl = document.createElement("p");
            filters[19].addFilterToDOM(pLvl);
            filters[20].addFilterToDOM(pLvl);
            var pUniform = document.createElement("p");
            filters[4].addFilterToDOM(pUniform);
            filters[21].addFilterToDOM(pUniform);
            var pClothesHigh = document.createElement("p");
            filters[12].addFilterToDOM(pClothesHigh);
            filters[22].addFilterToDOM(pClothesHigh);
            var pClothesDown = document.createElement("p");
            filters[13].addFilterToDOM(pClothesDown);
            filters[23].addFilterToDOM(pClothesDown);
            var pShoes = document.createElement("p");
            filters[24].addFilterToDOM(pShoes);
            var pOther = document.createElement("p");
            pOther.textContent = "Autres :";
            var pBack = document.createElement("p");
            filters[5].addFilterToDOM(pBack);
            filters[26].addFilterToDOM(pBack);
            var pHead = document.createElement("p");
            filters[14].addFilterToDOM(pHead);
            filters[25].addFilterToDOM(pHead);
            var pTruc = document.createElement("p");
            filters[15].addFilterToDOM(pTruc);
            var pSpirit = document.createElement("p");
            filters[6].addFilterToDOM(pSpirit);
            filtersDOM.appendChild(pCosmetique);
            filtersDOM.appendChild(pType);
            filtersDOM.appendChild(pHair);
            filtersDOM.appendChild(pEyes);
            filtersDOM.appendChild(pNoze);
            filtersDOM.appendChild(pMouth);
            filtersDOM.appendChild(pEar);
            filtersDOM.appendChild(pSkinColor);
            filtersDOM.appendChild(pVestimentaire);
            filtersDOM.appendChild(pSchool);
            filtersDOM.appendChild(pLvl);
            filtersDOM.appendChild(pUniform);
            filtersDOM.appendChild(pClothesHigh);
            filtersDOM.appendChild(pClothesDown);
            filtersDOM.appendChild(pShoes);
            filtersDOM.appendChild(pOther);
            filtersDOM.appendChild(pBack);
            filtersDOM.appendChild(pHead);
            filtersDOM.appendChild(pTruc);
            filtersDOM.appendChild(pSpirit);
        }
    }
    drawSWFObj("");
}

function drawSWFObj(code) {
    var so = new SWFObject(
        "/static/swf/viewer.swf?v=43", 
        document.getElementById("persos_viewer"),
        199, 322, 8, "#1a1612");
    if (code == null)
        so.addVariable('face',
            document.getElementById("avatarType").value + ';' +
            document.getElementById("squeletron").value + ';' +
            document.getElementById("school").value + ';' +
            document.getElementById("lvl").value + ';' +
            document.getElementById("uniformPattern").value + ';' +
            document.getElementById("backPattern").value + ';' +
            document.getElementById("spiritRelated").value + ';' +
            document.getElementById("hair").value + ';' +
            document.getElementById("eyes").value + ';' +
            document.getElementById("noze").value + ';' +
            document.getElementById("mouth").value + ';' +
            document.getElementById("ear").value + ';' +
            document.getElementById("clothesHighType").value + ';' +
            document.getElementById("clothesDownType").value + ';' +
            document.getElementById("headAdd").value + ';' +
            document.getElementById("drop").value + ';' +
            document.getElementById("skinColor").value + ';' +
            document.getElementById("hairColor").value + ';' +
            document.getElementById("eyesColor").value + ';' +
            document.getElementById("uniformColor").value + ';' +
            document.getElementById("uniformBorderColor").value + ';' +
            document.getElementById("uniformPatternColor").value + ';' +
            document.getElementById("clothesHighColor").value + ';' +
            document.getElementById("clothesDownColor").value + ';' +
            document.getElementById("shoesColor").value + ';' +
            document.getElementById("pyramColor").value + ';' +
            document.getElementById("backColorPattern").value) ;
    else
        so.addVariable('face', code);
    so.addVariable('other', '0');
    so.addVariable('pa', '3') ;
    so.addVariable('paMax', '3') ;
    so.addVariable('grade', '0') ;
    so.addVariable('elem', ' 6 ');
    so.addVariable('xp', '0;0;0');
    so.addVariable('gold', '5');
    so.addVariable('token', '9');
    so.addVariable('ftoken', '1');
    so.addParam("scale", "noscale") ;
    so.addVariable('dom', "/static/") ;
    so.addVariable('pxp', 'Remplie à');
    so.addVariable('c', 'null');
    so.addParam('menu', 'false');
    so.addParam("AllowScriptAccess","always");
    so.addParam("wmode","transparent");
    so.write("swf_persos_viewer");
}